Welcome to your tutorial repository!
================

The earlier version of this page had a lot of other details about using Atlassian's git client which I have removed. I just want to use this as a git tutorial. 

A Quick git Tutorial
=========

This is going to be a simple introduction to the basics of `git`. Everything mentioned here is for a unix-based machine and all commands are run on a terminal. For other settings, google it. 

This tutorial is built as a sequence of instructions. Go through all of them and you would already have a decent understand of the basics of git. 

[TOC]

# Zero-th steps

**Step 0.1.** Install `git` (duh!)

On my ubuntu machine, I would do this by running

```
#!console
$ sudo apt-get install git
```

**Step 0.2.** Tell `git` your name and email address. All changes that you make would be tagged by these credentials. 

```
#!console

$ git config --global user.name "Ramprasad Saptharishi"
$ git config --global user.email "ramprasad@cmi.ac.in"
```

**Step 0.3.** *[Optional]* I prefer to use `emacs` as my text-editor. If you wish `git` to default to `emacs` whenever it requires some input from you (say like a commit message etc.), you could do this:
````
#!console

$ git config --global core.editor emacs
````

**Step 0.4.** *[Optional, but recommended]* Generate ssh-keys for pushing changes to bitbucket. You can create ssh-keys by running
````
#!console

$ ssh-keygen -t rsa

````
If you already have ssh keys created, it would prompt you to ask if you wish to overwrite your key. You can say 'no' if it does.
Now, copy the contents of `~/.ssh/id_rsa.pub` and paste them in the appropriate place in your bitbucket settings. (You would find them in your account settings under 'SSH Keys'.)


# Creating a copy of the repository

When you create your bitbucket account, you would have a "tutorial" repository. You can create a local copy of this repository by running the following command (after changing 'dasarpmar' to your username of course). 
````
#!console

$ git clone git@bitbucket.org:dasarpmar/tutorial.git
````
Even if you do not have a bitbucket account yet, you can still run the following command to clone a copy of my repository. 


If can rename the created folder if you wish. If you did not enter do **Step 0.4**, you would have to enter your bitbucket username and password here.

Now that we have a copy of the local repository, let us get used to some of the commands here. First, let us look at how we can review changes.

# Reviewing changes

To begin with, type `git status`. You would get an output something like this:
````
On branch master
Your branch is up-to-date with 'origin/master'.

nothing to commit, working directory clean
````
We will soon see what *branch* means, but for now it suffices to know that *master* is the default branch and *origin* is the repository on bitbucket.org.

To see what has happened so far, you could run `git log`. This will give you a list of all changes that have happened so far with the commit messages, who made the change, when etc. For a more concise log, you could run `git log --oneline`. I prefer to run the following command `git log --oneline --decorate --all --graph --date-order` as it gives more information.


```
#!console

$ git log --oneline --decorate --all --graph --date-order
* a71503a (HEAD, origin/master, origin/HEAD, master) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
```
The strange string in the beginning of the line is what you would use to as a reference to a state of the repository at that time.  The above log says that HEAD, origin/master and master are all in state `a71503a`.

I use this command so often that I have a shortcut `git l` for this. You can have one too by doing
````
#!console
$  git config --global alias.l 'log --oneline --decorate --all --graph --date-order'
````

# Visiting earlier versions

Let us fix some vocabulary now. The term *origin* refers to the repository on the bitbucket.org server. The term *local copy* will refer to your copy of the repository, and *working directory* would refer to your current working directory.

Now suppose you want to see what the repository was at the state `396ea28`, you just run the command
````
#!console
$ git checkout 396ea28
````
You can read what `git` says after you run this command. But in a nutshell, what this would do is change your entire working directory to the state as in `396ea28`. Note that this does not change your local copy of the repository. That is still (in this example) at `a71503a`.
````
#!console
$ git l
* a71503a (origin/master, origin/HEAD, master) Removing hackathon stuff from README
* 396ea28 (HEAD) One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
````
As of now, you are in what is called a 'detached head' state which is a state without a branch. Let us not get into details of what it means, but just keep in mind that this is a sort of a state where you can visit if you want but changes are sort of useless here.

You can go back to the master branch by running
````
#!console
$ git checkout master
````
Note that this achieves two things. You go back to the state `a71503a` and more importantly you are now on a *branch* which is master. Running `git checkout a71503a` will get your working directory to the same state but you would continue to be in a detached state.

You could also use the `git checkout` command to just visit an earlier version of a single file.

````
#!console
$ git checkout 396ea28 README.md
````
The local copy of README.md would now be replaced by what it was in state `396ea28`. Again, note that this does not change the state of the local repository. The way `git` sees it is -- "Okay, I am in state `a71503a` but the current working directory seems to have a different version of README.md". See for yourself:
````
#!console
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

  modified:   README.md
````

You can get back the version of README.md by running 



```
#!console

$ git checkout master README.md


```


This just replaces the working directory with the file README.md in the current state of 'master'.

Now let us move on to actually making changes.

# Making changes

We could just start making changes etc but the *master* branch is supposed to be ... well the master branch. Let us not mess with that. Let us start by creating a new branch to do all our tests in.

````
#!console
$ git branch testing
$ git checkout testing
````
The first command just creates a new branch *testing* from where you are. The second command switches to the branch *testing*. This is like copying all the file contents into a new folder called 'testing'. This way, whatever changes we make in this folder does not affect 'master'. 

There is also a command that creates a branch and moves switches to it : `git checkout -b testing`.
````
#!console
$ git status
On branch testing
nothing to commit, working directory clean
````
We can make whatever silly changes we want to make here. None of this would affect the master branch.

Let us make some changes on the README.md file.
````
#!console
$ #make some changes to README.md
$ git status
On branch testing
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

  modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
````

Now `git` sees that the working directory has been modified and asks you if these changes must be recorded. Recording changes is a two-step process in git. You first do `git add` and then `git commit` with a commit message.

````
#!console

$ git add README.md
$ git status
On branch test
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

  modified:   README.md

$ git commit -m "Made some silly change to the introduction"
[testing c124467] Made some silly change to the introduction
 1 file changed, 1 insertion(+), 1 deletion(-)
````
The last command `git commit -m "..."` is where you specify the commit message. You could also just run `git commit` and `git` would open up an editor for you to enter the commit message. (If you have set your default editor as in Step 0.3, then `git` would open that editor). One of the key reasons for having commits as a two-step process is that you would want to commit decent-sized sensible blocks of changes. For example, I keep making add-ing my changes until it becomes one modular set of changes and then I commit once.

Let us look at the log now.

````
#!console
$ git l
* c124467 (HEAD, testing) Made some silly change to the introduction
* a71503a (origin/master, origin/HEAD, master) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
````
As you can see, the branch *testing* is ahead of *master* by one commit. You can switch back to *master* by doing `git checkout master`, and return back to *testing* by doing `git checkout testing`. You may create as many branches as you like, and keep hopping between them as and when you please.

Note that whatever you do in the branch *testing* stays only there. Eventually, you would want to incorporate the changes you made on the *testing* branch to *master*.  This is when you do a `git merge`.

# Merging branches

A `git merge` is a way of applying changes on one branch to another. Let us take our setting:
````
#!console
$ git l
* c124467 (HEAD, testing) Made some silly change to the introduction
* a71503a (origin/master, origin/HEAD, master) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
````
As we can see, *testing* is one commit ahead of *master*. If we want to incorporate these changes into *master*, we first switch to the *master* branch and merge.
````
#!console
$ git checkout master
$ git merge testing
Updating a71503a..c124467
Fast-forward
 README.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
````
Running a `git l` tells us what the state of all branches are.
````
#!console
$ git l
* c124467 (HEAD, testing, master) Made some silly change to the introduction
* a71503a (origin/master, origin/HEAD) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
````

This is how I manage my workflow. I have my personal branch called *ramprasad* where I make all my changes. I create more branches if I feel I need them. Once I am satisfied with how my branch is, I eventually merge my branch with *master*. That way, the *master* branch always reflects a sensible version.

You don't have to really delete any branch. But if you really want to, you can do that using `git branch -d <branchname>`. This will allow you to delete a branch that has *already been merged*. If you just want to delete a branch regardless of whether or not it is merged, then do `git branch -D <branchname>`.


# A slightly more complicated merge

The earlier example was simple in the sense that when *testing* was one commit ahead of *master*, the branch *master* did not change. So naturally merging was easy -- just copy the reference *testing* to *master*. What is *master* also changes in the middle? Let us try an example. Again, let us create a new branch called *newtest* for this (instead of working with *master*).
````
#!console
$ git checkout -b newtest
````

Currently, both *testing* and *newtest* are at the same point. Let us go to *newtest* and make some change towards the end of the README.md file.

````
#!console
$ git checkout newtest
$  # make some changes to end of README.md
$ git add README.md
$ git commit -m "Made some changes towards end of README.md"
[newtest 5b3533a] Made some changes towards end of README.md
 1 file changed, 3 insertions(+)
````

Now let us switch to *testing* and make some change to the beginning of README.md
````
#!console
$ git checkout testing
$  # make some changes to beginning of README.md
$ git add README.md
$ git commit -m "Made some change towards beginning of README.md"
[testing 71ca177] Made some change towards beginning of README.md
 1 file changed, 1 insertion(+), 2 deletions(-)
````
Both branches *testing* and *newtest* have been changed. Let us look at the log now.
````
#!console
$ git l
* 71ca177 (HEAD, testing) Made some change towards beginning of README.md
| * 5b3533a (newtest) Made some changes towards end of README.md
|/  
* c124467 (master) Made some silly change to the introduction
* a71503a (origin/master, origin/HEAD) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time

````
Doesn't that look pretty! It shows you that both *testing* and *newtest* have changed in different ways. Can we merge? Technically, we should be able to since the changes in *testing* and *newtest* are on very different parts of the file. Let us try merging the changes on *newtest* with the branch *testing*
````
#!console
$ git checkout testing
$ git merge newtest
````
And you see a text editor open to enter a commit message. It would by default just say that the branches are being merged but you can add more to the message if you want. Save the file and close and the merge is complete. And it is done! Check the log again.
````
#!console
$ git l
*   367ec39 (HEAD, testing) Merge branch 'newtest' into testing
|\  
| * 5b3533a (newtest) Made some changes towards end of README.md
* | 71ca177 Made some change towards beginning of README.md
|/  
* c124467 (master) Made some silly change to the introduction
* a71503a (origin/master, origin/HEAD) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
````
The branches have been merged. Although both branches had changed before the merge, `git` can understand that the merge can be resolved without any issues. Note that although *testing* has the changes from *newtest*, the branch *newtest* still does not have the changes from *testing*. You can do that in a similar fashion.
````
#!console
$ git checkout newtest
$ git merge testing
Updating 5b3533a..367ec39
Fast-forward
 README.md | 3 +--
 1 file changed, 1 insertion(+), 2 deletions(-)
````
Now both *testing* and *newtest* have both the changes.

# Merge with conflicts

Let us now consider a scenario where the same line gets changed in two branches.
````
#!console
$ git checkout testing
$  # make a silly change on some line
$ git add README.md
$ git commit -m "Made some silly change for merge conflict"
[testing a9f857c] Made some silly change for merge conflict
 1 file changed, 1 insertion(+), 1 deletion(-)

$ git checkout newtest
$  # make a silly change on the same line
$ git add README.md
$ git commit -m "Made some silly change on same line for merge conflict"
[newtest 7c84298] Made some silly change on same line for merge conflict
 1 file changed, 1 insertion(+), 1 deletion(-)

$ git l
* 7c84298 (HEAD, newtest) Made some silly change on same line for merge conflict
| * a9f857c (testing) Made some silly change for merge conflict
|/  
*   367ec39 Merge branch 'newtest' into testing
|\  
| * 5b3533a Made some changes towards end of README.md
* | 71ca177 Made some change towards beginning of README.md
|/  
* c124467 (master) Made some silly change to the introduction
* a71503a (origin/master, origin/HEAD) Removing hackathon stuff from README
* 396ea28 One more attempt to fix the link
* 3cf8974 Fixing link in opening text.
* 839d043 Updated herding text and link.
* d53283c Updated README file for hackathon participants.
* 7ae3c2a adding all the content files for the first time
````
Let us try merging changes on *newtest* to the branch *testing*.
````
#!console
$ git checkout testing
$ git merge newtest
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
````
There, `git` does not know how the changes should be merged since two different changes happened on the same line. Nevertheless, `git` would have pointed out in README.md where it got confused. Let us open that file and see what that looks like. Around the region where both branches made changes, you see something like this
````
<<<<<<< HEAD
Welcome to this tutorial repository!
=======
Welcome to your awesome tutorial repository!
>>>>>>> newtest
````
This is `git`'s way of saying one of the branches had the first line, and the second had the other line. It is up to us to figure out what we want the result of the merge. Let us edit that to
````
Welcome to this awesome tutorial repository!
````
Save the file, close the file, and commit the changes.
````
#!console
$ git add README.md
$ git commit -m "Fixed merge conflicts"
[testing 67af0b9] Fixed merge conflicts
````
That's it. You have now successfully resolved the conflicts and merged the branches. You can check the log to find out.

# Keeping in sync with *origin*

So far, we have been playing with only our local repository. Eventually, we will need to sync changes either to or from the repository on bitbucket.org. Recall this is called *origin*. To get all changes from *origin*, you run this command.
````
#!console
$ git fetch origin
````
All changes in the *origin*'s *master* branch would now be locally at *origin/master*. You can then merge this with your *master*.
````
#!console
$ git checkout master
$ git merge origin/master
````
These two commands may also be combined into one command by `git pull origin master` but remember to run this command on the *master* branch. This might be simpler but it is always a good practice to do a fetch+merge rather than a pull. That way, you can inspect the changes before merging, if you wish to.

This is how you stay up to date with any changes in *origin*.

Let us now look at how we can send changes from our local *master* to *origin*. This is achieved with a command called `git push`.
````
#!console
$ git checkout master
$ git push origin master
````
Although this is the gist of the command, it is always a good practice to ensure that you are up date with *origin/master* before pushing changes. Normally, when I do a push, I do this:
````
#!console
$ git checkout master
$ git fetch origin
$ git merge origin/master
$ # resolve merge conflicts if any
$ git push origin master
````

Congratulations! You've done all the basics! [Optional] You can jump into the [Bitbucket 101](https://confluence.atlassian.com/x/cgozDQ) or you can check out [Atlassian's Git site](https://www.atlassian.com/git/) and learn more about Git workflows. But by now, you should have a decent understanding of the basic commands. Let me end with a small exercise. 

**Exercise**: Go to the 'Source' section from the link on the left, and download a copy of this README.md. In your local copy, update your README.md with this file, commit and push the change to your 'tutorial' repository. 


# Slightly more advanced stuff

There are three other commands in `git` that I use often: `git rebase`, `git reset` and `git cherry-pick`. I will highly recommend checking out Atlassian's tutorial on [rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing), [resetting](https://www.atlassian.com/git/tutorials/resetting-checking-out-and-reverting), or this tutorial on [cherry-picking](http://think-like-a-git.net/sections/rebase-from-the-ground-up/cherry-picking-explained.html).

----